/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Blackjack;

import java.util.Scanner;

/**
 *
 * @author Kunj Prajapati
 */
public class Blackjack {
    
    public static void main(String[] args) {
        
        System.out.println("Welcome to Blackjack!");
        
        
        Deck playingDeck = new Deck();
        playingDeck.createFull();
        playingDeck.shuffle();
        
        Deck playerDeck = new Deck();
        
        Deck dealerDeck = new Deck();
        
        
       double playerMoney = 100.00;
       
       Scanner in = new Scanner(System.in); 
       
       while(playerMoney >0) {
           
           System.out.println("You have $ " + playerMoney + ", how much would you like to bet?");
           double playerBet = in.nextDouble();
           if(playerBet > playerMoney) {
               System.out.println("You cannot bet more than you have. Please leave.");
               break;
           }
           
           boolean endRound = false;
           
           playerDeck.draw(playingDeck);
           playerDeck.draw(playingDeck);
           
           dealerDeck.draw(playingDeck);
           dealerDeck.draw(playingDeck);
           
           while(true) {
               System.out.println("Your hand:");
               System.out.println(playerDeck.toString());
               System.out.println("Your deck is valued at: " + playerDeck.cardValue());
               
               System.out.println("Dealer Hand: " + dealerDeck.getCard(0).toString()+ " and [hidden] ");
               
               System.out.println("Would you like to (1)Hit or (2) Stand ?");
               int response = in.nextInt();
               
               if(response == 1) {
                   playerDeck.draw(playingDeck);
                   System.out.println("You draw a: " + playerDeck.getCard(playerDeck.deckSize()-1).toString());
                   
                   if(playerDeck.cardValue() >21 ) {
                       System.out.println("Bust. Currntly valued at: " + playerDeck.cardValue());
                   playerMoney -= playerBet;
                   endRound=true;
                   break;
               }
           }
            if(response ==2) {
                break;
            }
       }
       System.out.println("Dealer Cards : " + dealerDeck.toString());
       if((dealerDeck.cardValue() > playerDeck.cardValue()) && endRound == false ) {
           System.out.println("Dealer beats you! ");
           playerMoney -= playerBet;
           endRound = true;
       }
       
       while((dealerDeck.cardValue() <17 && endRound==false)) {
           dealerDeck.draw(playingDeck);
            System.out.println("Dealer Draws " + dealerDeck.getCard(dealerDeck.deckSize()-1).toString());
           
          }
        System.out.println("Dealer's hand is valued at: " + dealerDeck.cardValue());
        
//         
         if((dealerDeck.cardValue() > 21 ) && endRound == false ){
            System.out.println("Dealer busts !! you win. ");
            playerMoney += playerMoney;
            endRound = true;
            
        }
       
//         
        if((dealerDeck.cardValue() == playerDeck.cardValue()) && endRound == false ){
            System.out.println("Push");
            endRound = true;
        }
        
//        if((playerDeck.cardValue() > dealerDeck.cardValue())&& endRound = false) 
           if((playerDeck.cardValue() > dealerDeck.cardValue()) && endRound == false ){
            
            System.out.println("You win the hand ! ");
            playerMoney += playerBet;
            endRound=true;
            
        }
        
       playerDeck.moveAllDeck(playingDeck);
       dealerDeck.moveAllDeck(playingDeck);
       System.out.println("End of Hand.");
       
       
       }
           
        
       System.out.println("Game over ! YOU are out of money. :( ");
       
       
        
    }
    
}
