/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Blackjack;

import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author 16476
 */
public class Deck {
    
    private ArrayList<Card> cards;
    
    public Deck(){
        this.cards=new ArrayList<Card>();
    }
    
    public void createFull(){
        
        for(Suit cardSuit : Suit.values()) {
            for(Value cardValue : Value.values()) {
                this.cards.add(new Card(cardSuit,cardValue));
            }
        }
    }
    
    public void shuffle(){
        ArrayList<Card> tmpDeck = new ArrayList<Card>();
        
        Random random = new Random();
        int randomCardIndex=0;
        int originalSize = this.cards.size();
        
        for (int i=0; i< originalSize;i++) {
            randomCardIndex = random.nextInt((this.cards.size()-1-0)+1) +0;
            tmpDeck.add(this.cards.get(randomCardIndex));
            
            this.cards.remove(randomCardIndex);
            
        } 
   
        this.cards=tmpDeck;
       
    }
    
    public String toString(){
        String cardListOutput= "";
      
        for(Card aCard : this.cards){
            cardListOutput += "\n " + aCard.toString();
           
        }
    return cardListOutput;
    
    }
    
    public void removeCard(int i) {
        this.cards.remove(i);
    }
    
    public Card getCard(int i){
        return this.cards.get(i);
    }
    
    public void addCard(Card addCard) {
        this.cards.add(addCard);
    }
    
    public void draw(Deck comingFrom) {
        this.cards.add(comingFrom.getCard(0));
        comingFrom.removeCard(0);
    }
    
    public int cardValue(){
        int totalvalue=0;
        int aces = 0;
        
        for (Card aCard : this.cards) {
            switch(aCard.getValue()) {
                case  TWO : totalvalue +=2; break;
                case  THREE : totalvalue +=3; break;
                case  FOUR : totalvalue +=4; break;
                case  FIVE : totalvalue +=5; break;
                case  SIX : totalvalue +=6; break;
                case  SEVEN : totalvalue +=7; break;
                case  EIGHT : totalvalue +=8; break;
                case  NINE : totalvalue +=9; break;
                case  TEN : totalvalue +=10; break;
               case  JACK : totalvalue +=10; break;
               case  QUEEN : totalvalue +=10; break;
               case  KING : totalvalue +=10; break;
               case  ACE : totalvalue += 1; break;
             }
        }
            for (int i=0; i<aces; i++) {
                if (totalvalue > 10) {
                    totalvalue +=1;
                }
                else {
                    totalvalue += 11;
                }
            }
           return totalvalue;
    }
    
    public int deckSize(){
        return this.cards.size();
    }
    
    public void moveAllDeck( Deck moveto) {
        int thisDeckSize=this.cards.size();
        
        for (int i=0 ; i< thisDeckSize; i++) {
            moveto.addCard(this.getCard(i));
        }
        
        for (int i=0 ; i< thisDeckSize; i++) {
            this.removeCard(0);
        }
       
    }
}
    
